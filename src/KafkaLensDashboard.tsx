import "./App.css"
import styled from "styled-components"
import Header from "./components/header/Header"
import { KafkaAdmin } from "./components/kafkaadmin/KafkaAdmin"
import { Producer } from "./components/producer/Producer"

const Dashboard = styled.div`
  text-algin: center;

  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  color: white;
`

function KafkaLensDashboard() {
  return (
    <Dashboard>
      <Header siteTitle="Kafka Lens"></Header>

      <KafkaAdmin></KafkaAdmin>
      <Producer></Producer>
    </Dashboard>
  )
}

export default KafkaLensDashboard
