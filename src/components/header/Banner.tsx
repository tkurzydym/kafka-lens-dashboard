import React from "react"
import MediaQuery from "react-responsive"
import { StyledBanner } from "./StyledHeader"

type BannerProps = {
  siteTitle: string
}

export const Banner = ({ siteTitle }: BannerProps) => {
  return (
    <StyledBanner>
      <MediaQuery maxWidth={950}>KL</MediaQuery>
      <MediaQuery minWidth={951}>{siteTitle}</MediaQuery>
    </StyledBanner>
  )
}

export default Banner
