import { StyledHeader } from "./StyledHeader"
import { Banner } from "./Banner"

type HeaderProps = {
  siteTitle: string
}

const Header = ({ siteTitle }: HeaderProps) => (
  <StyledHeader>
    <Banner siteTitle={siteTitle} />
  </StyledHeader>
)

export default Header
