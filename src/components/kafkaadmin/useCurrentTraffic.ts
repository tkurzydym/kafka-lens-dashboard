import { useState, useEffect } from "react"

export const useCurrentTraffic = () => {
  const [traffic, setTraffic] = useState([] as TrafficEvent[])

  useEffect(() => {
    var trafficSource = new EventSource(
      "http://localhost:8080/consumer/traffic"
    )

    trafficSource.addEventListener("allTopics2", event => {
      console.log("Received Event!!")
      console.log(event)
      const messageEvent = event as MessageEvent
      addEventToTraffic(JSON.parse(messageEvent.data))
    })
  }, [])

  const addEventToTraffic = (event: TrafficEvent) => {
    setTraffic(oldState => [event, ...oldState])
  }

  return {
    traffic,
  }
}

export type TrafficEvent = {
  topic: String
  key: String
  type: String
  time: Date
}
