import { useState, useEffect } from "react"

const defaultTopics = [
  { topicName: "topic-1", numberOfPartitions: 1 },
  { topicName: "topic-2", numberOfPartitions: 1 },
] as TopicDescription[]

export const useKafkaAdmin = () => {
  const [topics, setTopics] = useState(defaultTopics)

  useEffect(() => {
    window
      .fetch("/admin/topics")
      .then(response => response.json())
      .then((topics: TopicDescription[]) => setTopics(topics))
  }, [])

  return {
    topics,
  }
}

export type TopicDescription = {
  topicName: string
  numberOfPartitions: number
}
