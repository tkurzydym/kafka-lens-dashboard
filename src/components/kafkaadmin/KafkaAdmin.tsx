import { TrafficEvent, useCurrentTraffic } from "./useCurrentTraffic"
import { useKafkaAdmin } from "./useKafkaAdmin"
import Moment from "react-moment"
import styled from "styled-components"

const StyledTrafficFrame = styled.div`
  margin: 1rem;
  border: 1px;
  border-color: rgb(50, 50, 50);
  border-style: solid;
  border-radius: 0.5rem;
  background-color: rgb(30, 30, 30);
`

const StyledTopicFrame = styled.div`
  margin: 0.5rem;
  padding: 1rem;
  border: 1px;
  border-color: rgb(50, 50, 50);
  border-style: solid;
  border-radius: 0.5rem;
  background-color: rgb(30, 30, 30);
`

const TopicContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(min(200px, 100%), 2fr));
`

const TrafficContainer = styled.div`
  display: grid;
  // grid-template-columns: repeat(auto-fill, minmax(min(200px, 100%), 2fr));
  grid-template-columns: repeat(auto-fit, minmax(min(200px, 100%), 2fr));
`

const Traffic = styled.span`
  margin: 1rem;
  justify-self: center;
`

export const KafkaAdmin = () => {
  const { topics } = useKafkaAdmin()
  const { traffic } = useCurrentTraffic()

  return (
    <div>
      <h1>Available Topics:</h1>

      <TopicContainer>
        {topics.map(desc => {
          return (
            <StyledTopicFrame key={desc.topicName}>
              <p>
                <b>{desc.topicName}</b>
              </p>
              <p>{desc.numberOfPartitions} partition(s)</p>
            </StyledTopicFrame>
          )
        })}
      </TopicContainer>

      <h1>Current Traffic on the Kafka Server:</h1>
      {traffic.map((kafkaEvent: TrafficEvent) => {
        return (
          <StyledTrafficFrame
            key={kafkaEvent.key + "-" + kafkaEvent.time?.toString()}
            style={{
              borderColor: getColor(kafkaEvent.type), //TODO to be randomized
            }}
          >
            <TrafficContainer>
              <Traffic>Topic: {kafkaEvent.topic}</Traffic>
              <Traffic>Event: {kafkaEvent.type}</Traffic>
              <Traffic>
                <Moment
                  date={kafkaEvent.time}
                  format="DD.MM.YYYY HH:mm:ss.SSS"
                />
              </Traffic>
              <Traffic>Key: {kafkaEvent.key || "<No key provided>"}</Traffic>
            </TrafficContainer>
          </StyledTrafficFrame>
        )
      })}
    </div>
  )
}

function getColor(type: String): string {
  return intToHsl(getHashCode(type !== null ? type : ""))
}

const getHashCode = function (value: String): number {
  var hash = 0
  if (value.length === 0) return hash
  for (var i = 0; i < value.length; i++) {
    hash = value.charCodeAt(i) + ((hash << 5) - hash)
    hash = hash & hash
  }
  return hash
}

const intToHsl = function (number: number): string {
  var shortened = number % 360
  return "hsl(" + shortened + ",100%,30%)"
}
