import JSONInput from "react-json-editor-ajrm"
import locale from "./JsonInputLocale"
import { useKafkaAdmin, TopicDescription } from "../kafkaadmin/useKafkaAdmin"
import React, { useState } from "react"
import styled from "styled-components"

const ProducerContainer = styled.div`
  display: grid;
  grid-template-columns: [first] 40px [line2] auto [line3] auto [line4] 40px [end];
  grid-template-rows: [row1] auto [row2] auto [last-line];
`

const LeftColumn = styled.div`
  grid-column: 2;
  grid-row-start: row2;
`
const RightColumn = styled.div`
  grid-column: 3;
  grid-row-start: row2;
`

const Input = styled.input``

const InputElement = styled.div`
  input[type="text"],
  select,
  button {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  button {
    background-color: green;
    border-color: green;
    color: white;
    &:hover {
      background-color: lightgreen;
    }
  }

  button:active {
    background-color: darkgreen;
  }

  button:disabled,
  button[disabled] {
    background-color: lightgrey;
  }

  padding: 12px 20px;
`
export const Producer = () => {
  const { topics } = useKafkaAdmin()

  const [useCloudEvent, setUseCloudEvent] = useState(false)
  const [selectedTopic, setSelectedTopic] = useState("")
  const [eventType, setEventType] = useState("")
  const [eventKey, setEventKey] = useState("")

  const [eventAsStringData, setEventAsStringData] = useState("")

  const handleEventTypeUpdate = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setEventType(event.target.value)
  }

  const handleEventKeyUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEventKey(event.target.value)
  }

  return (
    <ProducerContainer>
      <div
        style={{
          gridRowStart: "row1",
          paddingBottom: "1rem",
          gridColumn: "line2",
        }}
      >
        <h1>Producer</h1>
        <span>JSON Payload: </span>
      </div>

      <LeftColumn>
        <JSONInput
          id="kafka-payload"
          locale={locale}
          width="100%"
          height="550px"
          onChange={({ json }: { json: string }) => setEventAsStringData(json)}
        />
        <p>Current Data: {eventAsStringData}</p>
      </LeftColumn>

      <RightColumn>
        <InputElement>
          <select
            onChange={event => setSelectedTopic(event.target.value)}
            defaultValue=""
            value={selectedTopic}
          >
            <option value="" disabled>
              --- select your topic ---
            </option>
            {topics.map(({ topicName }: TopicDescription) => {
              return (
                <option key={topicName} value={topicName}>
                  {topicName}
                </option>
              )
            })}
          </select>
        </InputElement>

        <InputElement>
          <Input
            type="text"
            placeholder="Event Type"
            value={eventType}
            onChange={handleEventTypeUpdate}
          />
        </InputElement>

        <InputElement>
          <input
            type="text"
            placeholder="Event Key"
            value={eventKey}
            onChange={handleEventKeyUpdate}
          ></input>
        </InputElement>

        <InputElement>
          <span>Use Cloud Event Format </span>
          <input
            type="checkbox"
            onChange={() => setUseCloudEvent(!useCloudEvent)}
          ></input>
        </InputElement>

        <InputElement>
          <button
            disabled={selectedTopic === ""}
            onClick={() => {
              var body: CloudEventDTO = {
                type: eventType,
                source: "kafka-lens",
                time: new Date(),
                data: eventAsStringData,
              }
              window.fetch(
                "/producer/" +
                  selectedTopic +
                  "?wrapAsCloudEvent=" +
                  useCloudEvent,
                {
                  method: "POST",
                  headers: { "Content-Type": "application/json" },
                  body: JSON.stringify(body),
                }
              )
            }}
          >
            Send Event
          </button>
        </InputElement>
      </RightColumn>
    </ProducerContainer>
  )
}

type CloudEventDTO = {
  type: string
  source: string
  time: Date
  data: string
}
